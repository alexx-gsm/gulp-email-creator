const path = require('path');
const {src, dest, series, parallel} = require('gulp');
const mustache = require("gulp-mustache");
const watch = require('gulp-watch');
const inlineCss = require('gulp-inline-css');
const htmlbeautify = require('gulp-html-beautify');
const data = require('./src/templates/data/data.json');

const TEMPLATES_PATH = 'src/templates';

// HTML: render (w/ tag applying styles, only link styles)
const inlineCssOptions = {
    applyStyleTags: false,
    removeStyleTags: false,
    applyLinkTags: true,
    removeLinkTags: true,
    applyTableAttributes: true,
    removeHtmlSelectors: true
}

function defaultTask() {
    return src(path.resolve(__dirname, `${TEMPLATES_PATH}/*.mustache`))
        .pipe(mustache(data,{extension: ".html"},{}))
        .pipe(inlineCss(inlineCssOptions))
        .pipe(dest("./html"));
}

// HTML: render
const inlineCssRenderHtmlOptions = {
    applyStyleTags: true,
    removeStyleTags: true,
    applyLinkTags: true,
    removeLinkTags: true,
    applyTableAttributes: true,
    removeHtmlSelectors: true
}

function renderHtml() {
    return src(path.resolve(__dirname, `${TEMPLATES_PATH}/*.mustache`))
        .pipe(mustache(data,{extension: ".html"},{}))
        .pipe(inlineCss(inlineCssRenderHtmlOptions))
        .pipe(dest("./render"));
}

// TEMPLATES: apply styles
const inlineCssBuildOptions = {
    applyStyleTags: false,
    removeStyleTags: false,
    applyLinkTags: true,
    removeLinkTags: true,
    applyTableAttributes: true,
    removeHtmlSelectors: true
}

function build() {
    return src(path.resolve(__dirname, `${TEMPLATES_PATH}/*.mustache`))
        .pipe(inlineCss(inlineCssBuildOptions))
        .pipe(htmlbeautify())
        .pipe(dest("./build"));
}

// PARTIALS: apply styles
const inlineCssPartialsBuildOptions = {
    applyStyleTags: true,
    removeStyleTags: true,
    applyTableAttributes: true,
    removeHtmlSelectors: true
}

function buildPartials() {
    return src(path.resolve(__dirname, `${TEMPLATES_PATH}/partials/*.mustache`))
        .pipe(inlineCss(inlineCssPartialsBuildOptions))
        .pipe(htmlbeautify())
        .pipe(dest("./build/partials"));
}

// WATCHER
function w() {
    watch(['./src/**/*.mustache', './src/**/*.css', './src/templates/data/*.*'], defaultTask);
}

exports.default = defaultTask;

exports.render = renderHtml;

exports.dev = series(defaultTask, w);

exports.build = parallel(build, buildPartials);

// AMP
const AMP_PATH = 'src/amp';

function ampRender() {
    const ampData = require('./src/amp/data.json');

    return src(path.resolve(__dirname, `${AMP_PATH}/*.mustache`))
        .pipe(mustache(ampData,{extension: ".html"},{}))
        .pipe(dest(AMP_PATH));
}

function ampWatch() {
    watch(['./src/amp/**/*.mustache', './src/amp/data.json'], ampRender);
}

exports.amp_dev = series(ampRender, ampWatch);
// exports.amp_build =
